#pragma once

class AudioRecorder  : public AudioIODeviceCallback
{
public:
  AudioRecorder (AudioThumbnail& thumbnailToUpdate);
  ~AudioRecorder();
  
  void startRecording (const File& file);
  
  void stop();
  
  bool isRecording() const;
  
  
  void audioDeviceAboutToStart (AudioIODevice* device) override;
  
  void audioDeviceStopped() override;
  
  void audioDeviceIOCallback (const float** inputChannelData, int numInputChannels,
                              float** outputChannelData, int numOutputChannels,
                              int numSamples) override;
  
private:
  AudioThumbnail& thumbnail;
  TimeSliceThread backgroundThread { "Audio Recorder Thread" };
  std::unique_ptr<AudioFormatWriter::ThreadedWriter> threadedWriter; // the FIFO used to buffer the incoming data
  double sampleRate = 0.0;
  int64 nextSampleNum = 0;
  
  CriticalSection writerLock;
  std::atomic<AudioFormatWriter::ThreadedWriter*> activeWriter { nullptr };
};