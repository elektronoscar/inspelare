#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioLiveScrollingDisplay.h"
#include "CustomLookAndFeel.h"
#include "AudioRecorder.h"


//==============================================================================
class RecordingThumbnail : public Component, private ChangeListener
{
public:
  RecordingThumbnail()
  {
    formatManager.registerBasicFormats();
    thumbnail.addChangeListener (this);
  }
  
  ~RecordingThumbnail()
  {
    thumbnail.removeChangeListener (this);
  }
  
  AudioThumbnail& getAudioThumbnail()     { return thumbnail; }
  
  void setDisplayFullThumbnail (bool displayFull)
  {
    displayFullThumb = displayFull;
    repaint();
  }
  
  void paint (Graphics& g) override
  {
    g.fillAll (Colour(0xff101010));
    g.setColour (Colour(0xfff0f0f0));
    
    if (thumbnail.getTotalLength() > 0.0)
    {
      auto endTime = displayFullThumb ? thumbnail.getTotalLength()
      : jmax (30.0, thumbnail.getTotalLength());
      
      auto thumbArea = getLocalBounds();
      thumbnail.drawChannels (g, thumbArea.reduced (2), 0.0, endTime, 1.0f);
    }
    else
    {
      g.setFont (Font(14.0f));
      g.drawFittedText ("(No file recorded)", getLocalBounds(), Justification::centred, 2);
    }
  }
  
private:
  AudioFormatManager formatManager;
  AudioThumbnailCache thumbnailCache  { 10 };
  AudioThumbnail thumbnail            { 512, formatManager, thumbnailCache };
  
  bool displayFullThumb = false;
  
  void changeListenerCallback (ChangeBroadcaster* source) override
  {
    if (source == &thumbnail)
      repaint();
  }
  
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RecordingThumbnail)
};


class MainComponent   : public AudioAppComponent
{
public:
  MainComponent();
  ~MainComponent();

  void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
  void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
  void releaseResources() override;

  void paint (Graphics& g) override;
  void resized() override;

private:
  AudioDeviceSelectorComponent deviceSelectorComponent;
  
  LiveScrollingAudioDisplay liveAudioScroller;
  RecordingThumbnail recordingThumbnail;
  AudioRecorder recorder  { recordingThumbnail.getAudioThumbnail() };
  
  Label explanationLabel  { {},
#if (JUCE_ANDROID || JUCE_IOS)
    "After you are done with your recording you can share with other apps."
#else
    "Pressing record will start recording a file in your \"Documents\" folder."
#endif
  };
  TextButton recordButton { "Record" };
  File lastRecording;
  
  void startRecording();
  void stopRecording();
  
  TextButton showSettings{"Settings"};
  Label titleLabel{{}, "Elektron Record"};
  
  CustomLookAndFeel lookAndFeel;
  ScopedPointer<Drawable> logo{ Drawable::createFromImageData(BinaryData::LogoElektronWhite_svg, BinaryData::LogoElektronWhite_svgSize)};
  
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
