#include "MainComponent.h"



MainComponent::MainComponent()
  : deviceSelectorComponent(deviceManager, 1, 256, 0, 256, false, false, true, true)
{
  setLookAndFeel(&lookAndFeel);
 
  setOpaque (true);
  addAndMakeVisible (deviceSelectorComponent);
  deviceSelectorComponent.setVisible(false);
  addAndMakeVisible (liveAudioScroller);
  
  liveAudioScroller.setColours(Colour(0xff101010), Colour(0xfff0f0f0));
  
  addAndMakeVisible (explanationLabel);
  explanationLabel.setFont (Font (15.0f, Font::plain));
  explanationLabel.setJustificationType (Justification::topLeft);
  explanationLabel.setEditable (false, false, false);
  explanationLabel.setColour (TextEditor::textColourId, Colours::black);
  explanationLabel.setColour (TextEditor::backgroundColourId, Colour (0x00000000));
  
  
  const Colour redLight{0xffc87474};
  const Colour redDark{0xff553636};
  const Colour redOther{0xff2c2222};
  
  addAndMakeVisible (recordButton);
  recordButton.setColour (TextButton::buttonColourId, redLight);
  recordButton.setColour (TextButton::textColourOnId, Colours::black);
  
  recordButton.onClick = [this]
  {
    if (recorder.isRecording())
      stopRecording();
    else
      startRecording();
  };
  
  addAndMakeVisible (recordingThumbnail);
  
  RuntimePermissions::request (RuntimePermissions::recordAudio,
                               [this] (bool granted)
                               {
                                 int numInputChannels = granted ? 2 : 0;
                                 deviceManager.initialise (numInputChannels, 2, nullptr, true, {}, nullptr);
                               });
  
  deviceManager.addAudioCallback (&liveAudioScroller);
  deviceManager.addAudioCallback (&recorder);
  
  addAndMakeVisible(showSettings);
  showSettings.setClickingTogglesState(true);
  
  showSettings.onClick = [this]() {
    deviceSelectorComponent.setVisible(!deviceSelectorComponent.isVisible());
    liveAudioScroller.setVisible(!liveAudioScroller.isVisible());
    recordingThumbnail.setVisible(!recordingThumbnail.isVisible());
    explanationLabel.setVisible(!explanationLabel.isVisible());
    recordButton.setVisible(!recordButton.isVisible());;

    repaint();
  };
  
  addAndMakeVisible(titleLabel);
  titleLabel.setName("Title");
  
  setSize (500, 500);

  setAudioChannels (2, 2);
}

MainComponent::~MainComponent()
{
  
  deviceManager.removeAudioCallback (&recorder);
  deviceManager.removeAudioCallback (&liveAudioScroller);
  
  shutdownAudio();
  setLookAndFeel(nullptr);
}

void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
}

void MainComponent::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    bufferToFill.clearActiveBufferRegion();
}

void MainComponent::releaseResources()
{
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
  g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
  auto area = getLocalBounds().removeFromBottom(32).reduced(8);
  logo->drawWithin (g, area.toFloat(), RectanglePlacement::xRight, 1.0f);
}

void MainComponent::resized()
{
  auto area = getLocalBounds();

  auto topRow = area.removeFromTop(40);
  showSettings.setBounds(topRow.removeFromRight(80));
  titleLabel.setBounds(topRow);

  deviceSelectorComponent.setBounds(area.reduced(8));
  
  
  liveAudioScroller .setBounds (area.removeFromTop (80).reduced (8));
  recordingThumbnail.setBounds (area.removeFromTop (80).reduced (8));
  recordButton      .setBounds (area.removeFromTop (60).removeFromLeft (140).reduced (8));
  explanationLabel  .setBounds (area.reduced (8));
}

void MainComponent::startRecording()
{
  if (! RuntimePermissions::isGranted (RuntimePermissions::writeExternalStorage))
  {
    SafePointer<MainComponent> safeThis (this);
    
    RuntimePermissions::request (RuntimePermissions::writeExternalStorage,
                                 [safeThis] (bool granted) mutable
                                 {
                                   if (granted)
                                     safeThis->startRecording();
                                 });
    return;
  }
  
#if (JUCE_ANDROID || JUCE_IOS)
  auto parentDir = File::getSpecialLocation (File::tempDirectory);
#else
  auto parentDir = File::getSpecialLocation (File::userDocumentsDirectory);
#endif
  
  lastRecording = parentDir.getNonexistentChildFile ("Record recording", ".wav");
  
  recorder.startRecording (lastRecording);
  
  recordButton.setButtonText ("Stop");
  recordingThumbnail.setDisplayFullThumbnail (false);
}

void MainComponent::stopRecording()
{
  recorder.stop();
  
#if (JUCE_ANDROID || JUCE_IOS)
  SafePointer<MainComponent> safeThis (this);
  File fileToShare = lastRecording;
  
  ContentSharer::getInstance()->shareFiles (Array<URL> ({URL (fileToShare)}),
                                            [safeThis, fileToShare] (bool success, const String& error)
                                            {
                                              if (fileToShare.existsAsFile())
                                                fileToShare.deleteFile();
                                              
                                              if (! success && error.isNotEmpty())
                                              {
                                                NativeMessageBox::showMessageBoxAsync (AlertWindow::WarningIcon,
                                                                                       "Sharing Error",
                                                                                       error);
                                              }
                                            });
#endif
  
  lastRecording = File();
  recordButton.setButtonText ("Record");
  recordingThumbnail.setDisplayFullThumbnail (true);
  }
