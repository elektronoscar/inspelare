#pragma once

static const Font& getGrotesk55Font()
{
  static Font wacky (Font (Typeface::createSystemTypefaceFor (BinaryData::Linotype__NHaasGroteskDSPro55Rg_ttf,                       BinaryData::Linotype__NHaasGroteskDSPro55Rg_ttfSize)));
  return wacky;
}

static const Font& getGrotesk75Font()
{
  static Font wacky (Font (Typeface::createSystemTypefaceFor (BinaryData::Linotype__NHaasGroteskDSPro75Bd_ttf,                       BinaryData::Linotype__NHaasGroteskDSPro75Bd_ttfSize)));
  return wacky;
}

class CustomLookAndFeel : public LookAndFeel_V4 {
public:
    CustomLookAndFeel() {
      setColourScheme(LookAndFeel_V4::ColourScheme{
        0xff101010, /* windowBackground */
        0xff101010, /* widgetBackground */
        0xff505050, /* menuBackground */
        0xff505050, /* outline */
        0xfff0f0f0, /* defaultText */
        0xff808080, /* defaultFill */
        0xffffffff, /* highlightedText */
        0xff808080, /* highlightedFill */
        0xfff0f0f0  /* menuText */
      });
      setDefaultSansSerifTypeface (Typeface::createSystemTypefaceFor (BinaryData::Linotype__NHaasGroteskDSPro55Rg_ttf,                       BinaryData::Linotype__NHaasGroteskDSPro55Rg_ttfSize));
    }
  
  Font getTextButtonFont (TextButton &, int buttonHeight) override {
    return getGrotesk55Font();
  }
  
  Font getAlertWindowTitleFont () override {
    return getGrotesk75Font();
  }
  Font getAlertWindowMessageFont () override {
    return getGrotesk55Font();
  }
  Font getAlertWindowFont () override {
    return getGrotesk55Font();
  }
  Font getComboBoxFont (ComboBox &) override {
    return getGrotesk55Font();
  }
  
  Font getLabelFont (Label &l) override {
    if(l.getName() == "Title") {
      return getGrotesk75Font().withPointHeight(20.0f);
    }
    return getGrotesk55Font();
  }
};
