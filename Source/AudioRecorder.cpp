#include "AudioRecorder.h"


  AudioRecorder::AudioRecorder (AudioThumbnail& thumbnailToUpdate)
  : thumbnail (thumbnailToUpdate)
  {
    backgroundThread.startThread();
  }
  
  AudioRecorder::~AudioRecorder()
  {
    stop();
  }
  
  void AudioRecorder::startRecording (const File& file)
  {
    stop();
    
    if (sampleRate > 0)
    {
      file.deleteFile();
      
      if (auto fileStream = std::unique_ptr<FileOutputStream> (file.createOutputStream()))
      {
        WavAudioFormat wavFormat;
        
        if (auto writer = wavFormat.createWriterFor (fileStream.get(), sampleRate, 1, 16, {}, 0))
        {
          fileStream.release();

          threadedWriter.reset (new AudioFormatWriter::ThreadedWriter (writer, backgroundThread, 32768));
          
          thumbnail.reset (writer->getNumChannels(), writer->getSampleRate());
          nextSampleNum = 0;
          
          const ScopedLock sl (writerLock);
          activeWriter = threadedWriter.get();
        }
      }
    }
  }
  
  void AudioRecorder::stop()
  {
    {
      const ScopedLock sl (writerLock);
      activeWriter = nullptr;
    }
    
    threadedWriter.reset();
  }
  
  bool AudioRecorder::isRecording() const
  {
    return activeWriter.load() != nullptr;
  }
  
  
  void AudioRecorder::audioDeviceAboutToStart (AudioIODevice* device) 
  {
    sampleRate = device->getCurrentSampleRate();
  }
  
  void AudioRecorder::audioDeviceStopped() 
  {
    sampleRate = 0;
  }
  
  void AudioRecorder::audioDeviceIOCallback (const float** inputChannelData, int numInputChannels,
                              float** outputChannelData, int numOutputChannels,
                              int numSamples) 
  {
    const ScopedLock sl (writerLock);
    
    if (activeWriter.load() != nullptr && numInputChannels >= thumbnail.getNumChannels())
    {
      activeWriter.load()->write (inputChannelData, numSamples);
      
      AudioBuffer<float> buffer (const_cast<float**> (inputChannelData), thumbnail.getNumChannels(), numSamples);
      thumbnail.addBlock (nextSampleNum, buffer, 0, numSamples);
      nextSampleNum += numSamples;
    }
    
    for (int i = 0; i < numOutputChannels; ++i)
      if (outputChannelData[i] != nullptr)
        FloatVectorOperations::clear (outputChannelData[i], numSamples);
  }
  